# Ansible Role: docker

* Does **NOT** install docker or docker-compose
* Sets up additional docker stuff:
  * Prepares nginx config from a template
  * Prepares a docker-compose.yml from a template

## Requirements

  CentOS 7

## Role Variables

List of ssh keys to authorize for the user

Name of the user

```yml
maintenance_user: isleward
```

Folder where the templated files reside

```yml
isleward_config_folder: /etc/isleward
```

DNS name of the server

```yml
isleward_server_name: isleward.local
```

The port Isleward is running on

```yml
isleward_port: 4000
```

Path to Isleward's Docker image

```yml
isleward_container_image: registry.gitlab.com/isleward/isleward:master
```

Name of the Isleward container

```yml
isleward_container_id: isleward
```

Database used by Isleward

```yml
isleward_db: rethink
```

Hostname/Address of the database

```yml
isleward_db_host: rethinkdb
```

Port on which Isleward is communicating with the database

```yml
isleward_db_port: 28015
```

Path to the Docker config

```yml
docker_config_json: "/home/{{ maintenance_user }}/.docker/config.json"
```

The interval in seconds after which Ouroboros should check for new versions of images

```yml
ouroboros_interval: 60
```

## Dependencies

  None.

## Example Playbook

```yml
- hosts: localhost
  roles:
    - isleward.docker
```  

## Author Information

This role was created in 2018 by Vildravn.